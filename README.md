# Build an API Service

This doc contains instructions to setup and run code in this application.

## Technologies Used
- Python 3.5
- MySQL
- Redis
- -Docker?-

## Install dependencies
```bash
# On Debian systems
sudo apt install python3 mysql redis
```

## Setup Instructions

```bash
mysql -uroot -p -e 'create database plivo'
mysql -uroot -p plivo < plivo.sql

virtualenv venv -ppython3
venv/bin/pip3 install -r requirements.txt
./index.py

# Your server should be ready on localhost:5000
```
 
## How to run unittests
```bash
mysql -uroot -p -e 'create database plivo_test'
./test.py
```

## Status

The application is complete to the requirements stated. I decided to use Python 3.5 for this task, even though I have only 
recently begun using it. It took about a day to complete, mostly because I am not familiar with Flask.

The code is commented in key places and is formatted to PEP8 standards.

Your comments are welcome.

### Code Structure

```bash
~/workspace/plivo$ tree . -I '*pycache*|venv' --dirsfirst
.
├── plivo                   # The root namespace
│   ├── config
│   │   └── config.yml      # Config file
│   ├── controller
│   │   └── App.py          # The main Flask controller
│   ├── model               # Models for tables
│   │   ├── Account.py 
│   │   └── PhoneNumber.py
│   ├── test
│   │   └── AppTest.py      # Tests are written here
│   ├── BaseController.py   # Base class for controller
│   ├── FlaskApp.py         # App is initialized here
│   └── ServiceContainer.py # ServiceContainer. See code docs
├── index.py                # The app runner
├── Plivo-Coding-Assignment (1).pdf
├── plivo.sql
├── README.md
├── requirements.txt        # Python dependencies
└── test.py                 # The test runner

```

### What I've left out
- Logging
- Ensuring ServiceContainer reuses connections