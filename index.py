#!venv/bin/python3

if __name__ == '__main__':
    from plivo.FlaskApp import get_flask_app

    app = get_flask_app(__name__, 'default')

    app.run(debug=True)
