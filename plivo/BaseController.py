from flask_httpauth import HTTPBasicAuth
from plivo.ServiceContainer import ServiceContainer


class BaseController:
    """
    Abstract Base Class for controllers
    A concrete controller implementation should extend this
    """

    def __init__(self, container: ServiceContainer, auth: HTTPBasicAuth):
        self._auth = auth
        self._container = container

    def get_container(self):
        """
        Get the container
        :return: ServiceContainer
        """
        return self._container
