from flask import Flask, make_response, jsonify
from flask_httpauth import HTTPBasicAuth


def get_container(env):
    """
    Get a new instance of ServiceContainer
    
    :param env: The 'environment' to run the app. One of 'default', 'test'. See config/config.yml
    :return: ServiceContainer
    """

    from plivo.ServiceContainer import ServiceContainer
    import yaml
    with open("plivo/config/config.yml", "r") as infile:
        config = yaml.load(infile)
    return ServiceContainer(config[env])


def get_flask_app(name, env):
    """
    Create a Flask app using 'env'
    
    :param name: The name of the Flask app
    :param env: See get_container
    :return: Flask
    """

    # Setup Flask
    app = Flask(name)

    @app.errorhandler(404)
    def handler_404(error):
        return make_response(jsonify({'error': 'Not found'}), 404)

    @app.errorhandler(405)
    def handler_405(error):
        return make_response(jsonify({'error': 'Method not allowed'}), 405)

    # Create a service container using env
    container = get_container(env)

    # Setup auth
    auth = HTTPBasicAuth()

    @auth.get_password
    def _get_pw(username):
        from plivo.model.Account import Account
        account = Account(container.get_mysql_conn())
        return account.get_password(username)

    # Add application routes
    from plivo.controller.App import App
    controller = App(container, auth)

    app.add_url_rule('/', 'index', auth.login_required(controller.index), methods=['GET'])
    app.add_url_rule('/inbound/sms/', 'inbound_post', auth.login_required(controller.inbound_post), methods=['POST'])
    app.add_url_rule('/outbound/sms/', 'outbound_post', auth.login_required(controller.outbound_post), methods=['POST'])

    return app
