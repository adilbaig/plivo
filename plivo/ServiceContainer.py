class ServiceContainer:
    """
    A ServiceContainer provides access to external services, such as databases and caching systems.
    This object is initialized by the bootstrap script and loaded into the controller.
    """

    def __init__(self, config):
        self._config = config

    def get_mysql_conn(self):
        # if hasattr(self, '_mysql') is not True or self._mysql is None:
        import pymysql
        return pymysql.connect(**self._config['mysql'], cursorclass=pymysql.cursors.DictCursor)

    def get_redis(self):
        import redis
        return redis.StrictRedis(**self._config['redis'])
