from flask import request, jsonify
from plivo.BaseController import BaseController
from plivo.model.PhoneNumber import PhoneNumber


class App(BaseController):
    """
    This class implements the meat of the logic
    Each method represents a REST endpoint    
    """

    def index(self):
        str = """
        <h1>Welcome to the SMS App</h1>
        <ol>
            <li><a href="/inbound/sms/">POST /inbound/sms/</a></li>
            <li><a href="/outbound/sms/">POST /outbound/sms/</a></li>
        </ol>
        """
        return str

    def inbound_post(self):
        response = {'message': '', 'error': ''}
        res = self._validate_input(request, response)
        if res:
            return res

        try:
            phone_number = PhoneNumber(self._container.get_mysql_conn())
            is_valid = phone_number.is_number_valid(self._auth.username(), request.json['to'])
            if not is_valid:
                response['error'] = 'to parameter not found'
                return jsonify(response), 400

            import re
            match = re.search("^STOP[\\r\\n]{0,2}$", request.json['text'])
            if match is not None:
                r = self._container.get_redis()
                key = request.json['from'] + ':' + request.json['to']
                r.set(key, '1')
                r.expire(key, 4 * 60 * 60)

            response['message'] = 'inbound sms ok'
            return jsonify(response), 200
        except:
            response['error'] = 'unknown failure'
            return jsonify(response), 500

    def outbound_post(self):
        response = {'message': '', 'error': ''}
        res = self._validate_input(request, response)
        if res:
            return res

        try:
            phone_number = PhoneNumber(self._container.get_mysql_conn())
            is_valid = phone_number.is_number_valid(self._auth.username(), request.json['from'])
            if not is_valid:
                response['error'] = 'from parameter not found'
                return jsonify(response), 400

            r = self._container.get_redis()
            key = request.json['from'] + ':' + request.json['to']
            val = r.get(key)
            if val:
                response['error'] = "sms from {frm} to {to} blocked by STOP request".format(frm=request.json['from'],
                                                                                            to=request.json['to'])
                return jsonify(response), 400

            '''
            Using cache, do not allow more than 50 API requests using the same ‘from’ number in
            24 hours from the first use of the ‘from’ number and reset counter after 24 hours.
            '''
            key = 'from:' + request.json['from']
            requests = r.incr(key)
            if r.ttl(key) == 0:
                r.expire(key, 24 * 60 * 60)
            if requests > 50:
                response['error'] = "limit reached for from {frm}".format(frm=request.json['from'])
                return jsonify(response), 400

            response['message'] = 'output sms ok'
            return jsonify(response), 200
        except:
            response['error'] = 'unknown failure'
            return jsonify(response), 500

    @staticmethod
    def _validate_input(request, response):
        # Validate json
        for k in ('from', 'to', 'text'):
            if request.json is None or k not in request.json:
                response['error'] = '{} is missing'.format(k)
                return jsonify(response), 400

        # Validate attributes of request
        validate_len = {'to': [6, 16], 'from': [6, 16], 'text': [1, 120]}
        for k, a in validate_len.items():
            if not isinstance(request.json[k], str) or len(request.json[k]) < a[0] or len(request.json[k]) > a[1]:
                response['error'] = '{} is invalid'.format(k)
                return jsonify(response), 400
