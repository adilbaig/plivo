import pymysql


class Account:
    """
    Model that deals with accounts
    """

    def __init__(self, connection: pymysql.Connection):
        self._connection = connection

    def get_password(self, user: str):
        """
        Get the password for 'user'
        :param user: 
        :return: string or None
        """
        sql = "SELECT auth_id as password FROM account WHERE username = %s"
        cursor = self._connection.cursor()
        cursor.execute(sql, [user])
        result = cursor.fetchone()
        if result:
            return result['password']
