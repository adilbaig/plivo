import pymysql


class PhoneNumber:
    """
    Model that deals with phone_number
    """

    def __init__(self, connection: pymysql.Connection):
        self._connection = connection

    def is_number_valid(self, account_id, number):
        """
        Check if a 'number' is valid for 'account_id
        :param account_id: 
        :param number: 
        :return: boolean
        """
        sql = "SELECT p.number FROM phone_number p, account a WHERE a.id = p.account_id AND a.username = %s AND p.number = %s"
        cursor = self._connection.cursor()
        cursor.execute(sql, [account_id, number])
        result = cursor.fetchone()
        return result is not None
