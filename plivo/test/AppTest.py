import unittest
import json
from plivo.FlaskApp import get_flask_app, get_container


class AppTest(unittest.TestCase):
    """
    Implements unit and integration tests of the App
    All of the testing requirements are covered here.
    """

    def setUp(self):
        self._container = get_container('test')

        conn = self._container.get_mysql_conn()
        conn.autocommit(True)
        cursor = conn.cursor()

        sql = "DROP TABLE IF EXISTS account;"
        cursor.execute(sql)

        sql = "DROP TABLE IF EXISTS phone_number;"
        cursor.execute(sql)

        sql = """
        CREATE TABLE `account` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `auth_id` varchar(45) NOT NULL,
          `username` varchar(45) NOT NULL,
          PRIMARY KEY (`id`),
          UNIQUE KEY `username_UNIQUE` (`username`)
        ) ENGINE=InnoDB;
        """
        cursor.execute(sql)

        sql = "INSERT INTO `account` VALUES(1, '20S0KPNOIM', 'plivo1')"
        cursor.execute(sql)

        sql = """
        CREATE TABLE IF NOT EXISTS `phone_number` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `number` bigint(20) NOT NULL,
          `account_id` int(11) NOT NULL,
          PRIMARY KEY (`id`),
          KEY `account_id_idx` (`account_id`),
          CONSTRAINT `account_id` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
        ) ENGINE=InnoDB;
        """
        cursor.execute(sql)

        sql = "INSERT INTO `phone_number` VALUES(1, 123456789, 1),(2, 987654321, 1),(3, 11112222, 1)"
        cursor.execute(sql)

        app = get_flask_app(__name__, 'test')
        app.testing = True
        self.app = app.test_client()

    def tearDown(self):
        container = get_container('test')
        conn = container.get_mysql_conn()
        conn.autocommit(True)
        cursor = conn.cursor()

        sql = "DROP TABLE IF EXISTS phone_number;"
        cursor.execute(sql)

        sql = "DROP TABLE IF EXISTS account;"
        cursor.execute(sql)

        redis = container.get_redis()
        redis.flushdb()

    def runTest(self):
        # Put tests here so setUp and tearDown happens only once
        self.basic()
        self.inbound_sms()
        self.outbound_sms()

    def basic(self):
        assert self.app.get('/').status_code == 401
        assert self.app.get('/notfound').status_code == 404

    def inbound_sms(self):
        url = '/inbound/sms/'
        assert self.app.get(url).status_code == 405

        # NOTE: While your requirement states it should be 403, HTTPBasicAuth does not support that.
        # https://httpstatuses.com/401
        assert self.app.post(url).status_code == 401
        assert self.app.post(url, headers={"Authorization": "Basic junk"}).status_code == 401

        headers = {"Authorization": "Basic cGxpdm8xOjIwUzBLUE5PSU0=", "Content-Type": "application/json"}

        rv = self.app.post(url, data=json.dumps({"to": 1, "text": 1}), headers=headers)
        assert rv.status_code == 400
        assert json.loads(rv.data.decode('utf-8'))['error'] == "from is missing"

        rv = self.app.post(url, data=json.dumps({"from": "123456789", "to": 1, "text": "STOP"}), headers=headers)
        assert rv.status_code == 400
        assert json.loads(rv.data.decode('utf-8'))['error'] == "to is invalid"

        rv = self.app.post(url, data=json.dumps({"from": "123456789", "to": "987654", "text": "STOP"}), headers=headers)
        assert rv.status_code == 400
        assert json.loads(rv.data.decode('utf-8'))['error'] == "to parameter not found"

        j = {"from": "123456789", "to": "987654321", "text": "STOP"}
        rv = self.app.post(url, data=json.dumps(j), headers=headers)
        assert rv.status_code == 200
        assert json.loads(rv.data.decode('utf-8'))['error'] == ""
        assert json.loads(rv.data.decode('utf-8'))['message'] == "inbound sms ok"

        redis = self._container.get_redis()
        key = "{}:{}".format(j['from'], j['to'])
        assert redis.get(key) == b"1"

    def outbound_sms(self):
        url = '/outbound/sms/'
        assert self.app.get(url).status_code == 405

        # NOTE: While your requirement states it should be 403, HTTPBasicAuth does not support that.
        # https://httpstatuses.com/401
        assert self.app.post(url).status_code == 401
        assert self.app.post(url, headers={"Authorization": "Basic junk"}).status_code == 401

        headers = {"Authorization": "Basic cGxpdm8xOjIwUzBLUE5PSU0=", "Content-Type": "application/json"}

        rv = self.app.post(url, data=json.dumps({"to": 1, "text": 1}), headers=headers)
        assert rv.status_code == 400
        assert json.loads(rv.data.decode('utf-8'))['error'] == "from is missing"

        # If the pair ‘to’, ‘from’ matches any entry in cache (STOP):
        j = {"from": "123456789", "to": "987654321", "text": "Is this blocked?"}
        rv = self.app.post(url, data=json.dumps(j), headers=headers)
        assert rv.status_code == 400
        assert json.loads(rv.data.decode('utf-8'))['error'] == "sms from {} to {} blocked by STOP request".format(
            j['from'], j['to'])

        # If ‘from’’ is not found in the phone_number table for this account:
        j = {"from": "1234567899", "to": "987654321", "text": "Is this blocked?"}
        rv = self.app.post(url, data=json.dumps(j), headers=headers)
        assert rv.status_code == 400
        assert json.loads(rv.data.decode('utf-8'))['error'] == "from parameter not found"

        # Test if it blocks after 50 runs
        j = {"from": "11112222", "to": "987654321", "text": "Testing"}
        for i in range(0, 50):
            rv = self.app.post(url, data=json.dumps(j), headers=headers)
            assert json.loads(rv.data.decode('utf-8'))['message'] == "output sms ok"

        rv = self.app.post(url, data=json.dumps(j), headers=headers)
        assert rv.status_code == 400
        assert json.loads(rv.data.decode('utf-8'))['error'] == "limit reached for from {}".format(j['from'])
